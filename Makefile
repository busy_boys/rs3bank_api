venv_path = venv

docker_build:
	docker build -t rs3bank_api .
docker_stop:
	docker stop api || true
docker_rm:
	docker rm api || true

docker_run:
	docker run -d -l traefik.frontend.rule=Host:rstreebank.ru --restart=always --name api rs3bank_api
docker: docker_build docker_stop docker_rm docker_run
docker_run_dev:
	docker run -d --restart=always -p 8000:8000 --name api rs3bank_api
docker_dev: docker_build docker_stop docker_rm docker_run_dev

#start: SHELL:=/bin/bash
start:
	source ./$(venv_path)/bin/activate && gunicorn -w 2 -b 127.0.0.1:7878 entry:app

#dev: SHELL:=/bin/bash
dev:
	source ./$(venv_path)/bin/activate && export FLASK_APP=main.py FLASK_ENV=development && flask run --host 127.0.0.1 --port 7878

#create_req: SHELL:=/bin/bash
create_req:
	source ./$(venv_path)/bin/activate && pip freeze >> requirements.txt

#create_venv: SHELL:=/bin/bash
create_venv:
	python3 -m virtualenv $(venv_path) && source $(venv_path)/bin/activate && pip install --upgrade pip

