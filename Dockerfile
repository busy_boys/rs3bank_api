FROM python:2.7-alpine as BUILDER
WORKDIR /pre_app/

ADD rstviewer ./rstviewer

RUN apk add --update --no-cache build-base py-cryptography libffi-dev libressl-dev
RUN pip install --upgrade pip
RUN pip install pudb==2018.1 idna==2.10 cryptography==3.3.2 pyOpenSSL==21.0.0
RUN cd rstviewer; python setup.py install

RUN pip install natsort
ADD assets ./assets
ADD data data/
#RUN pip install beautifulsoup4
RUN python data/create_data.py

FROM python:3.7-alpine as RUNNER
EXPOSE 8000
WORKDIR /app/

RUN apk add --update --no-cache build-base

ADD requirements.txt .
RUN pip install -r requirements.txt

COPY --from=BUILDER /pre_app/assets ./assets
ADD main.py .

CMD gunicorn -w 4 -b 0.0.0.0:8000 main:app
