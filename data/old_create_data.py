#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from __future__ import print_function, with_statement, absolute_import

from rstviewer.main import cli
import zipfile
from os import chdir, getcwd, listdir, stat, mkdir, remove
from os import path as os_path
from json import dump
import re
from sys import stdout

# from bs4 import BeautifulSoup

global zip_file
zip_file = "RuRsTreebank_full.zip"
global zip_folder
zip_folder = "RuRsTreebank_full"


def check_location():
    """
    checks if you're in the right place for source directory, data directory
    and docker:BUILDER special case
    :return:
    True if in the right folder with 'RuRsTreebank_full_all.zip' is present
    """
    global script_path
    script_path = getcwd()
    print('source path =', script_path)
    if script_path == '/pre_app':
        print('running in Docker container(or edge case)')
        chdir('data')
    elif script_path.split('/')[-1] == 'rs3bank_api':
        print('running in source directory, chdir to "data"')
        chdir('data')
    elif script_path.split('/')[-1] == 'data':
        print('running in data directory no changes required')
    else:
        print('filesystem structure not found, start script from source directory')
        raise EnvironmentError
    try:
        stat(zip_file)
    except Exception:
        print(zip_file, " doesn't exist")
        raise EnvironmentError
    print(zip_file, ' exists, running scripts')
    return True


def get_data(schema):
    if schema is None:
        schema = {
            'news_texts': 'news',
            os_path.join('science_texts', 'compscience'): 'sci.ling',
            os_path.join('science_texts', 'linguistics'): 'sci.comp'
        }
    data = {}
    txts = {}
    rs3s = {}
    htmls = {}

    folder_type = ""
    for path, id_base in schema.items():
        chdir(os_path.join(script_path, zip_folder, path))
        for folder in listdir(getcwd()):
            chdir(folder)
            if 'rs3' in folder:
                folder_type = "rs3"
            elif 'txt' in folder:
                folder_type = "txt"
            else:
                print('folder type not found')
                raise EnvironmentError
            for file in listdir(getcwd()):
                # print(folder_type, file)
                id = id_base + '_' + file.replace('news_', '').replace('comp_', '').replace('ling_', '') \
                    .replace('.rs3', '').replace('.txt', '')
                if id not in data:
                    data[id] = {}
                with open(file, "r") as open_file:
                    txt = open_file.read()
                    if folder_type == 'rs3':
                        txt = re.sub(
                            r'<segment id="\d+" relname="\w+"><\/segment>',
                            '',
                            txt
                        )
                        rs3s[id] = txt
                        # soup = BeautifulSoup(txt, 'html.parser')
                        # txt = soup.decode_contents()
                        with open(file + '.new', 'w+') as out_file:
                            print(txt, file=out_file)
                        try:
                            cli([file + '.new', id + '.pre.html'])
                        except Exception:
                            stdout.write('\terror:\t' + file + '\n')
                        remove(file + '.new')
                        html = ""
                        with open(id + '.pre.html', 'r+') as old_file:
                            for line in old_file:
                                new_line = line#.replace('\n', '').replace('\t', '')
                                new_line = re.sub(
                                    r'<link rel="stylesheet" href="\/[\S*\/]*data\/css\/rst\.css" type="text\/css" charset="utf-8"\/>',
                                    '<link rel="stylesheet" href="/assets/css/rst.css" type="text/css" charset="utf-8"/>',
                                    new_line
                                )
                                new_line = re.sub(
                                    r'<script src="\/[\S\/]*data\/script\/jquery\-1\.11\.3\.min\.js"></script>',
                                    '<script src="/assets/script/jquery-1.11.3.min.js"></script>',
                                    new_line
                                )
                                new_line = re.sub(
                                    r'<script src="\/[\S\/]*data\/script\/jquery\-ui\.min\.js"></script>',
                                    '<script src="/assets/script/jquery-ui.min.js"></script>',
                                    new_line
                                )
                                new_line = re.sub(
                                    r'<script src="\/[\S\/]*data\/script\/jquery\.jsPlumb\-1\.7\.5\-min\.js"></script>',
                                    '<script src="/assets/script/jsPlumb-1.7.5-min.js"></script>',
                                    new_line
                                )
                                if new_line != '':
                                    html += new_line
                        remove(id + '.pre.html')
                        data[id]['html'] = html
                        htmls[id] = html
                    elif folder_type == 'txt':
                        txts[id] = txt
                    data[id][folder_type] = txt
            chdir('..')
    chdir(script_path)
    return data, rs3s, txts, htmls


def main():
    assert check_location(), True

    print('extracting .zip file', end="")
    zip_ref = zipfile.ZipFile(zip_file)
    zip_ref.extractall()
    zip_ref.close()
    print('\rextracted .zip file')

    print('processing files')
    schema = {
        'news_texts': 'news',
        os_path.join('science_texts', 'compscience'): 'sci.ling',
        os_path.join('science_texts', 'linguistics'): 'sci.comp'
    }
    data, rs3s, txts, htmls = get_data(schema=schema)
    print('processed files')

    print('saving files', end='')
    chdir(os_path.join('..', 'assets'))
    with open('data.json', 'wb+') as out_file:
        dump(data, out_file, ensure_ascii=False, indent=2)

    try:
        stat('files')
    except OSError:
        mkdir('files')
    chdir('files')
    for key, value in data.items():
        with open(key + '.json', 'wb+') as out_file:
            dump(value, out_file, ensure_ascii=False, indent=2)
    chdir('..')

    try:
        stat('rs3s')
    except OSError:
        mkdir('rs3s')
    chdir('rs3s')
    for key, value in rs3s.items():
        with open(key + '.rs3', 'wb+') as out_file:
            print(value, file=out_file)
    chdir('..')

    try:
        stat('txts')
    except OSError:
        mkdir('txts')
    chdir('txts')
    for key, value in txts.items():
        with open(key + '.txt', 'wb+') as out_file:
            print(value, file=out_file)
    chdir('..')

    try:
        stat('htmls')
    except OSError:
        mkdir('htmls')
    chdir('htmls')
    for key, value in htmls.items():
        with open(key + '.html', 'wb+') as out_file:
            print(value, file=out_file)
    chdir('..')

    global max_texts
    max_texts = 1000

    def custom_cmp(x):
        x_int = int(x.replace('sci.comp_', '').replace('sci.ling_', '').replace('news_', ''))
        if "sci.comp" in x:
            x_int += max_texts
        elif "sci.ling" in x:
            x_int += max_texts * 2
        elif "news" in x:
            pass
        else:
            print('\nunrecognized id\n')
        return x_int

    names = list(data.keys())
    names.sort(key=custom_cmp)
    with open('names.json', 'wb+') as out_file:
        dump(names, out_file, ensure_ascii=False, indent=2)


if __name__ == "__main__":
    main()
