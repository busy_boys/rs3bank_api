#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from __future__ import print_function, with_statement, absolute_import
import traceback

from rstviewer.main import cli
import zipfile
import natsort

import cgi
import os
from sys import stdout
import re
import json


def main():
    zip_location = correct_location()
    data = extract_data(zip_location)
    save_data(data)

def correct_location():
    path = os.getcwd()
    print('source path = ', path)
    if path == '/pre_app':
        print('running in Docker container')
        os.chdir('data')
    elif path.split('/')[-1] == 'rs3bank_api':
        print('running in source directory, chdir to "data"')
        os.chdir('data')
    elif path.split('/')[-1] == 'data':
        print('running in data directory(no changes required)')
    else:
        print('filesystem structure not recognized, start script from source directory of data folder')
        raise EnvironmentError
    path = os.getcwd()
    zip_file = os.path.join(path, "RuRsTreebank_full.zip")
    try:
        os.stat(zip_file)
    except OSError:
        print("zip file doesn't exist")
        raise OSError
    print('zip file exists, running scripts')
    return zip_file

def extract_data(zip_path):
    extracted = ''.join(zip_path.split('.')[:-1])
    res = {}
    zip_file = zipfile.ZipFile(zip_path)
    zip_file.extractall()
    zip_file.close()
    os.chdir(extracted)
    for folder_1 in os.listdir(extracted):
        if folder_1.startswith('README'):
            continue
        res_id = folder_1
        res[res_id] = {}
        os.chdir(folder_1)
        for folder_2 in os.listdir(os.getcwd()):
            if folder_2.endswith('txt'):
                tp = 'txt'
            elif folder_2.endswith('rs3'):
                tp = 'rs3'
            os.chdir(folder_2)
            for fl in os.listdir(os.getcwd()):
                name = '.'.join(fl.split('.')[:-1])
                file_id = name.split('_')[-1]
                if file_id not in res[res_id]:
                    res[res_id][file_id] = {}
                with open(fl, 'r') as in_file:
                    con = in_file.read()
                # con = con.decode('utf-8')
                res[res_id][file_id][tp] = con
                if tp == 'rs3':
                    con = re.sub(
                        r'<segment id="\d+" relname="\w+"><\/segment>',
                        '',
                        con
                    )
                    with open(fl + '.new', 'w+') as out_file:
                        print(con, file=out_file)
                    print(name, ' '*10, end='\r')
                    try:
                        cli([fl + '.new', name + '.html'])
                    except KeyError as e:
                        print('key error: ', name, '\n', e, file=stdout)
                        # print('key error: ', name, '\n', e, '\n', traceback.format_exc(), file=stdout)
                        # html = '<xmp>\n' + con + '\n</xmp>'
                        html = cgi.escape(con)
                    except IndexError as e:
                        print('index error: ', name, '\n', e, file=stdout)
                        # print('index error: ', name, '\n', e, '\n', traceback.format_exc(), file=stdout)
                        # html = '<xmp>\n' + con + '\n</xmp>'
                        html = cgi.escape(con)
                    else:
                        html = ''
                        with open(name + '.html', 'r') as html_file:
                            for line in html_file:
                                new_line = line#.replace('\n', '').replace('\t', '')
                                new_line = re.sub(
                                    r'<link rel="stylesheet" href="\/[\S*\/]*data\/css\/rst\.css" type="text\/css" charset="utf-8"\/>',
                                    '<link rel="stylesheet" href="/assets/css/rst.css" type="text/css" charset="utf-8"/>',
                                    new_line
                                )
                                new_line = re.sub(
                                    r'<script src="\/[\S\/]*data\/script\/jquery\-1\.11\.3\.min\.js"></script>',
                                    '<script src="/assets/script/jquery-1.11.3.min.js"></script>',
                                    new_line
                                )
                                new_line = re.sub(
                                    r'<script src="\/[\S\/]*data\/script\/jquery\-ui\.min\.js"></script>',
                                    '<script src="/assets/script/jquery-ui.min.js"></script>',
                                    new_line
                                )
                                new_line = re.sub(
                                    r'<script src="\/[\S\/]*data\/script\/jquery\.jsPlumb\-1\.7\.5\-min\.js"></script>',
                                    '<script src="/assets/script/jsPlumb-1.7.5-min.js"></script>',
                                    new_line
                                )
                                if new_line != '':
                                    html += new_line
                    os.remove(fl + '.new')
                    os.remove(name + '.html')
                    res[res_id][file_id]['rs3'] = con
                    res[res_id][file_id]['html'] = html
            os.chdir('..')
        os.chdir('..')
    os.chdir('..')
    print(' '*20)
    return res

def save_data(data):
    os.chdir(os.path.join('..', 'assets'))
    with open('data.json', 'wb+') as out_file:
        json.dump(data, out_file, ensure_ascii=False, indent=2)

    try:
        os.stat('files')
    except OSError:
        os.mkdir('files')
    os.chdir('files')

    names = []
    rs3s = {}
    txts = {}
    htmls = {}
    for data_id, data_con in data.iteritems():
        # with open(data_id + '.json', 'wb+') as out_file:
            # json.dump(data_con, out_file, ensure_ascii=False, indent=2)
        for text_id, trh in data_con.iteritems():
            name = data_id + '_' + text_id
            names.append(name)
            rs3s[name] = trh['rs3']
            txts[name] = trh['txt']
            htmls[name] = trh['html']
            with open(name + '.json', 'wb+') as out_file:
                json.dump(trh, out_file, ensure_ascii=False, indent=2)
    os.chdir('..')

    names = natsort.natsorted(names)
    with open('names.json', 'wb+') as out_file:
        json.dump(names, out_file, ensure_ascii=False, indent=2)

    try:
        os.stat('rs3s')
    except OSError:
        os.mkdir('rs3s')
    os.chdir('rs3s')
    for key, value in rs3s.iteritems():
        with open(key + '.rs3', 'wb+') as out_file:
            print(value, file=out_file)
    os.chdir('..')

    try:
        os.stat('txts')
    except OSError:
        os.mkdir('txts')
    os.chdir('txts')
    for key, value in txts.items():
        with open(key + '.txt', 'wb+') as out_file:
            print(value, file=out_file)
    os.chdir('..')

    try:
        os.stat('htmls')
    except OSError:
        os.mkdir('htmls')
    os.chdir('htmls')
    for key, value in htmls.items():
        with open(key + '.html', 'wb+') as out_file:
            print(value, file=out_file)
    os.chdir('..')
    return

if __name__ == '__main__':
    main()
